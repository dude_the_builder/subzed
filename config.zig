/// Server listening address and port.
pub const server_ip = "127.0.0.1";
pub const server_port = 8080;

/// Build options for the web server executable.
pub const cpu: Target.Cpu.Arch = .x86_64;
pub const abi: Target.Abi = .gnu;
pub const mode: Mode = .ReleaseSafe;

/// Cache-Control options (requires zig build freeze)
/// Max time in seconds an item can be cached
/// in the browser or proxy cache before requiring
/// revalidation via a request to the server.
pub const cache_max_age_small = 60 * 60 * 24; // 1 day
pub const cache_max_age_medium = 60 * 60 * 24 * 7; // 7 days
pub const cache_max_age_large = 60 * 60 * 24 * 30; // 30 days

/// File mime types and max age (requires zig build freeze)
pub const mime_cache_infos = StaticStringMap(ContentInfo).initComptime(.{
    .{ ".css", .{
        .mime_type = "text/css; charset=utf-8",
        .max_age = cache_max_age_small,
    } },
    .{ ".gif", .{
        .mime_type = "image/gif",
        .max_age = cache_max_age_medium,
    } },
    .{ ".html", .{
        .mime_type = "text/html; charset=utf-8",
        .max_age = cache_max_age_small,
    } },
    .{ ".js", .{
        .mime_type = "text/javascript",
        .max_age = cache_max_age_small,
    } },
    .{ ".json", .{
        .mime_type = "application/json",
        .max_age = cache_max_age_small,
    } },
    .{ ".jpeg", .{
        .mime_type = "image/jpeg",
        .max_age = cache_max_age_medium,
    } },
    .{ ".jpg", .{
        .mime_type = "image/jpeg",
        .max_age = cache_max_age_medium,
    } },
    .{ ".png", .{
        .mime_type = "image/png",
        .max_age = cache_max_age_medium,
    } },
    .{ ".svg", .{
        .mime_type = "image/svg+xml",
        .max_age = cache_max_age_medium,
    } },
    .{ ".webp", .{
        .mime_type = "image/webp",
        .max_age = cache_max_age_medium,
    } },
});

/// Files smaller than this in bytes will not
/// be compressed, no matter the file type.
pub const min_compress_size: u64 = 256;

/// If large enough, these file types will be compressed.
pub const compressible = StaticStringMap(void).initComptime(.{
    .{".css"},
    .{".html"},
    .{".js"},
    .{".json"},
    .{".txt"},
});

/// Server runtime options that control memory
/// usage and performance. Usually, you don't have
/// to modify these as they've been finely tuned
/// via benchmark testing.
pub const kernel_backlog = 16;
pub const client_buf_len = 1024;
pub const recv_buffers = 16;
pub const recv_buffer_len = 2048;
pub const recv_retries = 1;
pub const worker_threads = 12;

/// Request handling timeouts
pub const keep_alive_timeout_s = 1; // requires re-freeze
pub const recv_timeout_s = 1;
pub const send_timeout_s = 1;

/// io_uring specific options. Rarely need modification.
pub const main_ring_entries = 128;
pub const worker_ring_entries = 128;

// Data structures and utility functions. No edits
// needed past this point.

/// Mime type and cache-control max-age for content.
pub const ContentInfo = struct {
    mime_type: []const u8,
    max_age: u32,
};

/// Adds possible extension and removes slashes from filename.
pub fn cleanFilename(
    allocator: mem.Allocator,
    filename: []const u8,
    add_ext: ?[]const u8,
) ![]u8 {
    const ext_len = if (add_ext) |ext| ext.len else 0;
    var clean_filename = try allocator.alloc(u8, filename.len + ext_len);
    @memcpy(clean_filename[0..filename.len], filename);
    if (add_ext) |ext| @memcpy(clean_filename[filename.len..], ext);
    mem.replaceScalar(u8, clean_filename, '/', '_');
    return clean_filename;
}

/// Returns true if the file is large enough and has a
/// compressible file type.
pub fn shouldCompress(filename: []const u8, len: u64) bool {
    if (len < min_compress_size) return false;
    const ext = getExt(filename) orelse return false;
    return compressible.has(ext);
}

// Get the file extension, if any.
fn getExt(filename: []const u8) ?[]const u8 {
    return if (mem.lastIndexOfScalar(u8, filename, '.')) |dot|
        filename[dot..]
    else
        null;
}

const std = @import("std");
const StaticStringMap = std.StaticStringMap;
const mem = std.mem;
const Mode = std.builtin.Mode;
const Target = std.Target;
