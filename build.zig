const std = @import("std");
const Build = std.Build;
const fmt = std.fmt;
const fs = std.fs;
const mem = std.mem;

const config = @import("config.zig");

pub fn build(b: *Build) void {
    // Convention over configuration constants.
    const webroot_dir = "webroot";
    const codegen_file = "autogen.zig";

    // Command line option to select build mode for the main executable.
    const mode = b.option(std.builtin.Mode, "mode", "Optimization mode.") orelse config.mode;

    // Compress qualifying webroot files.
    // Build compress executable.
    const compress_exe = b.addExecutable(.{
        .name = "compress",
        .root_source_file = b.path("tools/compress.zig"),
        .target = b.host,
        .optimize = .Debug,
    });

    // Generate Zig code for processed web site files.
    // Build codegen executable.
    const codegen_exe = b.addExecutable(.{
        .name = "codegen",
        .root_source_file = b.path("tools/codegen.zig"),
        .target = b.host,
        .optimize = .Debug,
    });
    codegen_exe.root_module.addAnonymousImport("config", .{ .root_source_file = b.path("config.zig") });

    // Run codegen executable.
    const codegen_run = b.addRunArtifact(codegen_exe);
    codegen_run.addArg(webroot_dir);
    // Generated Zig code file.
    const codegen_output = codegen_run.addOutputFileArg(codegen_file);

    // Module for the generated Zig code, to be provided to
    // the main executable build.
    const codegen_mod = b.createModule(.{ .root_source_file = codegen_output });

    // Now we walk the webroot directory to process each file.
    var webroot = fs.cwd().openDir(webroot_dir, .{}) catch @panic("openIterableDir");
    defer webroot.close();
    var walker = webroot.walk(b.allocator) catch @panic("walk");
    defer walker.deinit();

    // File writer for files that don't need compression.
    const copy_write_files = b.addWriteFiles();

    while (walker.next() catch @panic("walker.next")) |entry| {
        if (entry.kind == .file) {
            // Need file size to determine if we should compress it.
            const stat = entry.dir.statFile(entry.basename) catch @panic("statFile failed");
            const should_compress = config.shouldCompress(entry.path, stat.size);
            const full_input_path = b.pathJoin(&.{ webroot_dir, entry.path });

            // Make codegen run if input file changes.
            codegen_run.addFileArg(b.path(full_input_path));

            // Clean output file name
            const add_ext: ?[]const u8 = if (should_compress) ".zz" else null;
            const clean_output_path = config.cleanFilename(
                b.allocator,
                entry.path,
                add_ext,
            ) catch @panic("cleanFilename");

            if (should_compress) {
                const compress_run = b.addRunArtifact(compress_exe);
                // Input file arg to keep in sync.
                compress_run.addFileArg(b.path(full_input_path));
                // Compressed file output.
                const compress_output = compress_run.addOutputFileArg(clean_output_path);
                // This makes the @embedFile calls work in the generated Zig file.
                codegen_mod.addAnonymousImport(clean_output_path, .{ .root_source_file = compress_output });
            } else {
                // No compression needed, just copy the original file.
                const copy_output = copy_write_files.addCopyFile(b.path(full_input_path), clean_output_path);
                // This makes the @embedFile calls work in the generated Zig file.
                codegen_mod.addAnonymousImport(clean_output_path, .{ .root_source_file = copy_output });
            }
        }
    }

    // The main web server executable.
    const subzed_exe = b.addExecutable(.{
        .name = "subzed",
        .root_source_file = b.path("src/main.zig"),
        .target = b.resolveTargetQuery(.{
            .cpu_arch = config.cpu,
            .os_tag = .linux,
            .abi = config.abi,
        }),
        .optimize = mode,
    });
    subzed_exe.root_module.addAnonymousImport("config", .{ .root_source_file = b.path("config.zig") });
    subzed_exe.root_module.addImport("autogen", codegen_mod);
    b.installArtifact(subzed_exe);

    const run_cmd = b.addRunArtifact(subzed_exe);
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| run_cmd.addArgs(args);

    const run_step = b.step("run", "Run the subzed web server.");
    run_step.dependOn(&run_cmd.step);
}
