const std = @import("std");

const Atomic = std.atomic.Value;
const builtin = @import("builtin");
const fmt = std.fmt;
const heap = std.heap;
const log = std.log;
const mem = std.mem;
const net = std.net;
const os = std.os;
const posix = std.posix;
const time = std.time;
const Thread = std.Thread;
const Uring = os.linux.IoUring;

const config = @import("config");
const autogen = @import("autogen");

var running = Atomic(bool).init(true);

pub fn main() !void {
    // Init logger
    const logger = std.log.scoped(.zws_main);

    // Handle OS signals
    try addSignalHandlers();

    // Setup thread pool
    var handles: [config.worker_threads]Thread = undefined;
    defer for (handles) |h| h.join();
    for (&handles) |*h| h.* = try Thread.spawn(.{}, logError, .{});

    logger.info("subzed listening on {s} port {}. CTRL+C to shutdown.", .{ config.server_ip, config.server_port });
}

fn logError() void {
    serve() catch |err| {
        std.log.err("error handling client request: {}", .{err});
        if (err == error.KernelTooOld) running.store(false, .release);
    };
}

// Ring ops
const Op = enum {
    accepted,
    close,
    recv,
    send,
};

// Client connection state
const Client = struct {
    op: Op,
    retries: u8,
    socket: posix.socket_t,
};

// 404
const not_found: []const u8 =
    \\HTTP/1.1 404 Not Found
    \\Content-Length: 10
    \\Content-Type: text/plain
    \\Server: zws/0.1.04 (linux)
    \\
    \\Not Found
    \\
;

fn serve() !void {
    const logger = log.scoped(.zws_handle_client);
    const thread_id = Thread.getCurrentId();

    // Per thread allocator
    var client_buf: [config.client_buf_len]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&client_buf);
    const allocator = fba.allocator();

    // Per thread ring
    var ring = try Uring.init(config.worker_ring_entries, 0);
    defer ring.deinit();

    // Provide buffers to receive requests.
    const recv_group_id = 2112;
    var recv_buffers: [config.recv_buffers][config.recv_buffer_len]u8 = undefined;
    var buffers_left: usize = config.recv_buffers;

    {
        _ = try ring.provide_buffers(
            0xbfbfbfbf,
            @ptrCast(&recv_buffers),
            config.recv_buffer_len,
            config.recv_buffers,
            recv_group_id,
            0,
        );
        _ = try ring.submit();

        const cqe = try ring.copy_cqe();
        switch (cqe.err()) {
            .SUCCESS => {},
            else => |err| {
                logger.err("error providing buffers: {}", .{err});
                return error.ProvideBuffers;
            },
        }
    }

    // Init listener
    var server_addr = try net.Address.parseIp(config.server_ip, config.server_port);
    const listener_flags = posix.SOCK.STREAM | posix.SOCK.CLOEXEC;
    const listener = try posix.socket(server_addr.any.family, listener_flags, 0);
    defer posix.close(listener);
    try posix.setsockopt(listener, posix.SOL.SOCKET, posix.SO.REUSEPORT, &mem.toBytes(@as(c_int, 1)));
    try posix.bind(listener, &server_addr.any, server_addr.getOsSockLen());
    try posix.listen(listener, config.kernel_backlog);

    // Start accepting
    _ = try ring.accept_multishot(0xacceacce, listener, null, null, 0);

    // Greaceful shutdown CTRL+C timeout
    const tick: os.linux.kernel_timespec = .{ .tv_sec = 2, .tv_nsec = 0 };
    _ = try ring.timeout(0xdeaddead, &tick, 0, 0);

    // Submit multishot and timeout.
    _ = try ring.submit();

    // Server loop
    while (running.load(.monotonic)) {
        // Get next completion
        const cqe = try ring.copy_cqe();

        // Timeout CQE
        if (cqe.user_data == 0xdeaddead) {
            _ = try ring.timeout(0xdeaddead, &tick, 0, 0);
            _ = try ring.submit();
            continue;
        }

        // Check for provide buffers CQE
        if (cqe.user_data == 0xbfbfbfbf) switch (cqe.err()) {
            .SUCCESS => {
                buffers_left = config.recv_buffers;
                continue;
            },

            else => |err| {
                logger.err("error providing buffers: {}", .{err});
                return error.ProvideBuffers;
            },
        };

        if (cqe.user_data == 0xacceacce) {
            switch (cqe.err()) {
                .SUCCESS => {
                    // Allocate and initialize client
                    const client_ptr = try allocator.create(Client);
                    client_ptr.* = .{
                        .retries = 0,
                        .socket = cqe.res,
                        .op = .accepted,
                    };
                    errdefer allocator.destroy(client_ptr);

                    // Submit SQE
                    _ = try ring.nop(@intFromPtr(client_ptr));

                    // Accept again if stopped.
                    if (cqe.flags & os.linux.IORING_CQE_F_MORE != os.linux.IORING_CQE_F_MORE) {
                        _ = try ring.accept_multishot(0xacceacce, listener, null, null, 0);
                    }

                    _ = try ring.submit();
                },

                else => |err| logger.err("error accepting: {}", .{err}),
            }

            continue;
        }

        // Get associated node pointer for user data
        const cqe_client_ptr: *Client = @ptrFromInt(cqe.user_data);
        errdefer {
            posix.close(cqe_client_ptr.socket);
            allocator.destroy(cqe_client_ptr);
        }

        // Check CQE errors
        switch (cqe.err()) {
            .SUCCESS => {},

            // Kernel doesn't support op
            .INVAL => {
                return error.KernelTooOld;
            },

            // Op finished before linked timeout
            .ALREADY => {
                logger.debug("{}: {s} {} finished before timeout", .{
                    thread_id,
                    @tagName(cqe_client_ptr.op),
                    cqe_client_ptr.socket,
                });
            },

            // Linked timeout finished first
            .TIME => {
                logger.debug("{}: {s} {} timeout", .{
                    thread_id,
                    @tagName(cqe_client_ptr.op),
                    cqe_client_ptr.socket,
                });

                // We only retry receives, send timeouts will
                // proceed as normal and try to receive again.
                if (cqe_client_ptr.op == .recv) {
                    cqe_client_ptr.retries += 1;

                    if (cqe_client_ptr.retries > config.recv_retries) {
                        logger.debug("{}: {s} {} {} retries, closing", .{
                            thread_id,
                            @tagName(cqe_client_ptr.op),
                            cqe_client_ptr.socket,
                            config.recv_retries,
                        });

                        // Too many retires, close the socket.
                        cqe_client_ptr.op = .close;
                        _ = try ring.close(cqe.user_data, cqe_client_ptr.socket);
                    } else {
                        logger.debug("{}: retry #{} recv from {}", .{
                            thread_id,
                            cqe_client_ptr.retries,
                            cqe_client_ptr.socket,
                        });

                        // Check buffers
                        buffers_left -|= 1;
                        if (buffers_left == 0) {
                            // Reprovide buffers
                            _ = try ring.provide_buffers(
                                0xbfbfbfbf,
                                @ptrCast(&recv_buffers),
                                config.recv_buffer_len,
                                config.recv_buffers,
                                recv_group_id,
                                0,
                            );
                            _ = try ring.submit();
                        }

                        // Try reading again
                        const sqe_recv = try ring.recv(
                            cqe.user_data,
                            cqe_client_ptr.socket,
                            .{ .buffer_selection = .{
                                .group_id = recv_group_id,
                                .len = config.recv_buffer_len,
                            } },
                            0,
                        );
                        // Link timeout
                        sqe_recv.flags |= os.linux.IOSQE_IO_LINK;
                        const ts = os.linux.kernel_timespec{ .tv_sec = config.recv_timeout_s, .tv_nsec = 0 };
                        _ = try ring.link_timeout(cqe.user_data, &ts, 0);
                    }

                    _ = try ring.submit();
                    continue;
                }
            },

            // Signal interrupt
            .INTR => {
                logger.debug("{}: {s} {} interrupt", .{
                    thread_id,
                    @tagName(cqe_client_ptr.op),
                    cqe_client_ptr.socket,
                });
                continue;
            },

            .CANCELED => {
                logger.debug("{}: {s} {} canceled", .{
                    thread_id,
                    @tagName(cqe_client_ptr.op),
                    cqe_client_ptr.socket,
                });
                continue;
            },

            else => |err| {
                logger.err("{}: CQE error: {}", .{ thread_id, err });
                return error.CompletionError;
            },
        }

        switch (cqe_client_ptr.op) {
            .accepted => {
                buffers_left -|= 1;
                if (buffers_left == 0) {
                    // Reprovide buffers
                    _ = try ring.provide_buffers(
                        0xbfbfbfbf,
                        @ptrCast(&recv_buffers),
                        config.recv_buffer_len,
                        config.recv_buffers,
                        recv_group_id,
                        0,
                    );
                    _ = try ring.submit();
                }

                // Prepare recv SQE
                cqe_client_ptr.op = .recv;
                const sqe_recv = try ring.recv(
                    cqe.user_data,
                    cqe_client_ptr.socket,
                    .{ .buffer_selection = .{
                        .group_id = recv_group_id,
                        .len = config.recv_buffer_len,
                    } },
                    0,
                );
                // Link timeout
                sqe_recv.flags |= os.linux.IOSQE_IO_LINK;
                const ts = os.linux.kernel_timespec{ .tv_sec = config.recv_timeout_s, .tv_nsec = 0 };
                _ = try ring.link_timeout(cqe.user_data, &ts, 0);
                // Submit
                _ = try ring.submit();
            },

            .close => {
                logger.debug("{}: Closed {}", .{ thread_id, cqe_client_ptr.socket });
                allocator.destroy(cqe_client_ptr);
            },

            .recv => {
                const buf_index = cqe.flags >> 16;

                if (cqe.res == 0) {
                    // Client closed connection
                    logger.debug("{}: Received 0, closing {}", .{ thread_id, cqe_client_ptr.socket });
                    cqe_client_ptr.op = .close;
                    _ = try ring.close(cqe.user_data, cqe_client_ptr.socket);
                    _ = try ring.submit();
                    continue;
                }

                const recv_len: usize = @intCast(cqe.res);
                logger.debug("{}: Received {} bytes from {}; buf_index: {}", .{
                    thread_id,
                    recv_len,
                    cqe_client_ptr.socket,
                    buf_index,
                });
                const req = recv_buffers[buf_index][0..recv_len];
                logger.debug("{}: recv buffer:\n{s}", .{ thread_id, req });

                // Prepare send SQE
                cqe_client_ptr.op = .send;
                var sqe_send: ?*os.linux.io_uring_sqe = null;

                const path = getPath(req) orelse blk: {
                    logger.debug("{}: no path, sendig 404", .{thread_id});
                    sqe_send = try ring.send(cqe.user_data, cqe_client_ptr.socket, not_found, 0);
                    break :blk "/index.html";
                };

                logger.debug("{}: path {s}", .{ thread_id, path });

                if (sqe_send == null) {
                    if (getEtag(req)) |etag| {
                        logger.debug("{}: etag {s}", .{ thread_id, etag });
                        if (autogen.etags.has(etag)) {
                            const responses = autogen.responses.get(path).?;
                            sqe_send = try ring.send(
                                cqe.user_data,
                                cqe_client_ptr.socket,
                                responses.not_modified,
                                0,
                            );
                        }
                    }
                }

                if (sqe_send == null) {
                    const resp = if (autogen.responses.get(path)) |responses|
                        responses.ok
                    else
                        not_found;
                    logger.debug("{}: response.len {}", .{ thread_id, resp.len });
                    sqe_send = try ring.send(cqe.user_data, cqe_client_ptr.socket, resp, 0);
                }

                // Link timeout
                sqe_send.?.flags |= os.linux.IOSQE_IO_LINK;
                const ts = os.linux.kernel_timespec{ .tv_sec = config.send_timeout_s, .tv_nsec = 0 };
                _ = try ring.link_timeout(cqe.user_data, &ts, 0);
                // Submit
                _ = try ring.submit();
            },

            .send => {
                logger.debug("{}: Send to {} res: {}", .{
                    thread_id,
                    cqe_client_ptr.socket,
                    cqe.res,
                });
                // When using keep-alive, we don't close; we read again.
                cqe_client_ptr.op = .accepted;
                _ = try ring.nop(cqe.user_data);
                _ = try ring.submit();
            },
        }
    }
}

fn getPath(bytes: []const u8) ?[]const u8 {
    var word_iter = mem.splitScalar(u8, bytes, ' ');
    _ = word_iter.next() orelse return null; // Method
    const path = word_iter.next() orelse return null;

    return if (path.len == 1 and path[0] == '/')
        "index.html"
    else
        path[1..];
}

fn getEtag(bytes: []const u8) ?[]const u8 {
    var line_iter = mem.split(u8, bytes, "\r\n");
    return while (line_iter.next()) |line| {
        if (mem.startsWith(u8, line, "If-None-Match")) {
            const startq = mem.indexOfScalar(u8, line, '"') orelse break null;
            if (startq == line.len - 1) break null;
            const endq = mem.lastIndexOfScalar(u8, line, '"') orelse break null;
            if (endq -| startq == 0) break null;
            break line[startq + 1 .. endq];
        }
    } else null;
}

fn addSignalHandlers() !void {
    const logger = log.scoped(.zws_signal);

    // Ignore broken pipes
    {
        var act = posix.Sigaction{
            .handler = .{
                .handler = posix.SIG.IGN,
            },
            .mask = posix.empty_sigset,
            .flags = 0,
        };
        try posix.sigaction(posix.SIG.PIPE, &act, null);
    }

    // Catch SIGINT/SIGTERM for proper shutdown
    {
        var act = posix.Sigaction{
            .handler = .{
                .handler = struct {
                    fn wrapper(sig: c_int) callconv(.C) void {
                        logger.info("Caught signal {d}; Shutting down...", .{sig});
                        running.store(false, .release);
                    }
                }.wrapper,
            },
            .mask = posix.empty_sigset,
            .flags = 0,
        };
        try posix.sigaction(posix.SIG.TERM, &act, null);
        try posix.sigaction(posix.SIG.INT, &act, null);
    }
}
