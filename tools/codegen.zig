const std = @import("std");
const compress = std.compress;
const fmt = std.fmt;
const fs = std.fs;
const hash = std.hash;
const heap = std.heap;
const io = std.io;
const mem = std.mem;
const time = std.time;

const WyHash = hash.Wyhash;
const config = @import("config");

fn walkDir(
    allocator: mem.Allocator,
    dir: *fs.Dir,
    etags_writer: anytype,
    codegen_writer: anytype,
) !void {
    var walker = try dir.walk(allocator);
    defer walker.deinit();

    while (try walker.next()) |entry| {
        if (entry.kind == .file) {
            var file = try dir.openFile(entry.path, .{});
            defer file.close();

            try processFile(
                allocator,
                entry.path,
                &file,
                etags_writer,
                codegen_writer,
            );
        }
    }
}

fn ctype(filename: []const u8) []const u8 {
    const aos = "application/octet-stream";

    return if (mem.lastIndexOfScalar(u8, filename, '.')) |dot|
        if (config.mime_cache_infos.get(filename[dot..])) |info| info.mime_type else aos
    else
        aos;
}

fn maxage(filename: []const u8) u32 {
    if (mem.lastIndexOfScalar(u8, filename, '.')) |dot| {
        if (config.mime_cache_infos.get(filename[dot..])) |info| return info.max_age;
    }

    return config.cache_max_age_small;
}

fn processFile(
    allocator: mem.Allocator,
    path: []const u8,
    file: *fs.File,
    etags_writer: anytype,
    codegen_writer: anytype,
) !void {
    // Prepare etag from mtime and path
    const seed = 350439712;
    const stat = try file.stat();
    var wh = WyHash.init(seed);
    wh.update(mem.asBytes(&stat.mtime));
    wh.update(path);
    const etag = wh.final();

    // Write etag
    try etags_writer.print(
        \\.{{ "{}" }},
        \\
    , .{etag});

    // Frozen file templates
    const ok_header_tpl =
        \\\\HTTP/1.1 200 OK
        \\\\Connection: Keep-Alive
        \\\\Keep-Alive: timeout={}
        \\\\Content-Type: {s}
        \\\\Content-Encoding: {s}
        \\\\Cache-Control: public, max-age={}, immutable
        \\\\Etag: "{}"
        \\\\Server: zws/0.1.0 (linux)
        \\\\
    ;

    const should_compress = config.shouldCompress(path, stat.size);
    const encoding: []const u8 = if (should_compress) "deflate" else "identity";
    const max_age = maxage(path);
    var ok_buf: [2048]u8 = undefined;
    const ok_header = try fmt.bufPrint(&ok_buf, ok_header_tpl, .{
        config.keep_alive_timeout_s,
        ctype(path),
        encoding,
        max_age,
        etag,
    });

    const not_mod_tpl =
        \\\\HTTP/1.1 304 Not Modified
        \\\\Connection: Keep-Alive
        \\\\Keep-Alive: timeout={}
        \\\\Content-Type: text/plain 
        \\\\Content-Length: 13
        \\\\Cache-Control: public, max-age={}, immutable
        \\\\Etag: "{}"
        \\\\Server: zws/0.1.0 (linux)
        \\\\
        \\\\Not Modified
        \\\\
    ;
    var not_mod_buf: [2048]u8 = undefined;
    const not_mod = try fmt.bufPrint(&not_mod_buf, not_mod_tpl, .{
        config.keep_alive_timeout_s,
        max_age,
        etag,
    });

    const responses_tpl =
        \\.{{ "{s}", .{{
        \\    .not_modified =
        \\        {s}
        \\    ,
        \\    .ok = blk: {{
        \\        const headers =
        \\            {s}
        \\        ;
        \\        const body = @embedFile("{s}");
        \\        break :blk std.fmt.comptimePrint("{{s}}Content-Length: {{}}\n\n{{s}}", .{{headers, body.len, body}});
        \\    }},
        \\}} }},
        \\
    ;

    // Clean body file name
    const add_ext: ?[]const u8 = if (should_compress) ".zz" else null;
    const body_filename = try config.cleanFilename(allocator, path, add_ext);
    defer allocator.free(body_filename);
    var responses_buf: [4096]u8 = undefined;
    const responses = try fmt.bufPrint(&responses_buf, responses_tpl, .{
        path,
        not_mod,
        ok_header,
        body_filename,
    });

    try codegen_writer.writeAll(responses);
}

pub fn main() !void {
    var gpa = heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var etags_list = std.ArrayList(u8).init(allocator);
    defer etags_list.deinit();
    const etags_writer = etags_list.writer();
    var codegen_list = std.ArrayList(u8).init(allocator);
    defer codegen_list.deinit();
    const codegen_writer = codegen_list.writer();

    var args_iter = std.process.args();
    _ = args_iter.next();
    const webroot_path = args_iter.next().?;

    var cwd = fs.cwd();

    var webroot = try cwd.openDir(webroot_path, .{});
    defer webroot.close();

    try walkDir(
        allocator,
        &webroot,
        etags_writer,
        codegen_writer,
    );

    const output_path = args_iter.next().?;

    var output_file = try cwd.createFile(output_path, .{});
    defer output_file.close();
    var buf_writer = io.bufferedWriter(output_file.writer());
    const writer = buf_writer.writer();

    try writer.writeAll(
        \\const std = @import("std");
        \\
        \\pub const etags = std.StaticStringMap(void).initComptime(.{
        \\
    );
    try writer.writeAll(etags_list.items);
    try writer.writeAll(
        \\});
        \\
        \\pub const Responses = struct {
        \\    not_modified: []const u8,
        \\    ok: []const u8,
        \\};
        \\
        \\pub const responses = std.StaticStringMap(Responses).initComptime(.{
        \\
    );
    try writer.writeAll(codegen_list.items);
    try writer.writeAll(
        \\});
        \\
    );

    try buf_writer.flush();
}
