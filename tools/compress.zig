const std = @import("std");
const compress = std.compress;
const fs = std.fs;
const io = std.io;
const heap = std.heap;

pub fn main() !void {
    var args_iter = std.process.args();
    _ = args_iter.skip();
    const input_path = args_iter.next().?;
    const output_path = args_iter.next().?;

    var cwd = fs.cwd();

    var input_file = try cwd.openFile(input_path, .{});
    defer input_file.close();
    var in_buf_reader = io.bufferedReader(input_file.reader());
    const reader = in_buf_reader.reader();

    var output_file = try cwd.createFile(output_path, .{});
    defer output_file.close();
    var out_buf_writer = io.bufferedWriter(output_file.writer());
    const writer = out_buf_writer.writer();

    var comp = try compress.flate.deflate.compressor(
        .raw,
        writer,
        .{ .level = .best },
    );

    const comp_writer = comp.writer();

    var buf: [4096]u8 = undefined;
    while (true) {
        const len = try reader.readAll(&buf);
        try comp_writer.writeAll(buf[0..len]);
        if (len < buf.len) break;
    }

    try comp.flush();
    try out_buf_writer.flush();
}
