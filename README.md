# subzed - High Performance Linux Static HTTP/1.1 Server

## This Project Needs a New Maintainer
If you would like to take over the development and maintenace of this project,
let me know by opening an issue.

`subzed` is a peculiar web server. It only supports static web site content,
and has quite a few draconian restrictions (see below). In exchange for these
limitations, you get an extremely high performance web server in a single, compact
executable binary file.

## How it Works

When you compile `subzed`, it processes the files in the `webroot` directory,
compressing if necessary and embedding them all in the generated web server 
binary executable file. This process generates the actual HTTP/1.1 responses that
will be served to client requests, so once the server is compiled, minimal processing
is required to serve those requests.

## How to Use

```sh
# Clone this repo
git clone https://codeberg.org/dude_the_builder/subzed.git
cd subzed

# Optionally set config options
vi config.zig

# Copy your static site to webroot
cp -a <static_site_path> webroot

# Compile the server
zig build

# Run the server (Linux only)
./zig-out/bin/subzed
```

Note that you can perform the build steps on a non-Linux machine, but the
final binary executable will only run on a Linux machine.

## Features

* High performance (see benchmark below).
* Simple to use; just put your site files in the `webroot` directory and compile.
* Single compact binary executable file.
* Single config file that lets you control:
  * Server IP and port.
  * Cache-Control expiry per file type.
  * MIME type per file type.
  * Compression size threshold and file types.
  * Server worker threads.
  * Server CPU architecture, ABI, and build mode.
  * Server send and receive timeouts.
  * Server memory usage settings.
  * Connection keep-alive timeout.

## Restrictions

Given the peculiarities of how `subzed` works, some limitations must be imposed:

* Linux kernels version 6 or newer only.
* HTTP/1.1 GET requests only.
* Unencrypted HTTP/1.1 traffic only.
* Static files only, no CGI, no dynamic processing.
* Single, small response only. No streaming or chunked transfers.
* Etag cache-control only. No date comparisons.
* No `Date` header in response.
* Deflate compression only.
* Keep-alive connections are required for high performance.

## Benchmark

The following results were obtained by running `subzed` on an AMD Ryzen 5 2600X 2 GHz
Six-Core Processor, Alpine Linux 3.19.0, Linux kernel 6.6.9-0-lts machine. The benchmark
tool is [`oha`](https://github.com/hatoo/oha). This is serving an `index.html` file of 9,888
bytes size.

```plain
$ ./oha -n 300000 -c 256 http://192.168.68.106:8080/index.html

Summary:
  Success rate: 100.00%
  Total:        1.1576 secs
  Slowest:      0.0367 secs
  Fastest:      0.0000 secs
  Average:      0.0010 secs
  Requests/sec: 259146.3258

  Total data:   759.03 MiB
  Size/request: 2.
  Size/sec:     655.67 MiB

Response time histogram:
  0.000 [1]      |
  0.004 [299531] |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  0.007 [314]    |
  0.011 [0]      |
  0.015 [0]      |
  0.018 [0]      |
  0.022 [0]      |
  0.026 [0]      |
  0.029 [0]      |
  0.033 [14]     |
  0.037 [140]    |

Response time distribution:
  10.00% in 0.0003 secs
  25.00% in 0.0005 secs
  50.00% in 0.0008 secs
  75.00% in 0.0013 secs
  90.00% in 0.0018 secs
  95.00% in 0.0020 secs
  99.00% in 0.0026 secs
  99.90% in 0.0042 secs
  99.99% in 0.0345 secs


Details (average, fastest, slowest):
  DNS+dialup:   0.0072 secs, 0.0001 secs, 0.0353 secs
  DNS-lookup:   0.0000 secs, 0.0000 secs, 0.0001 secs

Status code distribution:
  [200] 300000 responses
```

So roughly 259k requests per second on this not-so-new hardware (AMD Ryzen 5 2600x 2.2GHz),
not bad, eh? ;^)
